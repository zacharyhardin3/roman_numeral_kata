import math

class ArabicNumberConverter:
    def __extract_index_value(self, numerator, denominator):
        return numerator / denominator

    def __convert_remainder_into_whole_number(self, number, floating_point_value):
        return int(round(number * floating_point_value))

    def __convert_single_digit_numbers(self, number):
        array = ["", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"]

        return array[number]

    def __convert_double_digit_numbers(self, number):
        array = ["", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"]
        FLOATING_POINT_VALUE = 10

        remainder_number, whole_number = math.modf(self.__extract_index_value(number, FLOATING_POINT_VALUE))

        roman_numeral = array[int(whole_number)]
        roman_numeral += self.__convert_single_digit_numbers(
            self.__convert_remainder_into_whole_number(remainder_number, FLOATING_POINT_VALUE))

        return roman_numeral

    def __convert_triple_digit_numbers(self, number):
        array = ["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"]
        FLOATING_POINT_VALUE = 100

        remainder_number, whole_number = math.modf(self.__extract_index_value(number, FLOATING_POINT_VALUE))

        roman_numeral = array[int(whole_number)]
        roman_numeral += self.__convert_double_digit_numbers(
            self.__convert_remainder_into_whole_number(remainder_number, FLOATING_POINT_VALUE))

        return roman_numeral

    def __convert_quadruple_digit_numbers(self, number):
        array = ["", "M", "MM", "MMM", "MMMM", "MMMMM", "MMMMMM", "MMMMMMM", "MMMMMMMM", "MMMMMMMMM"]
        FLOATING_POINT_VALUE = 1000

        remainder_number, whole_number = math.modf(self.__extract_index_value(number, FLOATING_POINT_VALUE))

        roman_numeral = array[int(whole_number)]
        roman_numeral += self.__convert_triple_digit_numbers(
            self.__convert_remainder_into_whole_number(remainder_number, FLOATING_POINT_VALUE))

        return roman_numeral

    def convert_arabic_number_to_roman_numeral(self, number):
        if number < 10:
            return self.__convert_single_digit_numbers(number)
        elif number < 100:
            return self.__convert_double_digit_numbers(number)
        elif number < 1000:
            return self.__convert_triple_digit_numbers(number)
        elif number < 10000:
            return self.__convert_quadruple_digit_numbers(number)
        return "Your number is too large. Please select something between 1 and 10000."