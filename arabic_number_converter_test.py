import unittest
import nose.tools as nt
from arabic_number_converter import ArabicNumberConverter

class ArabicNumberConverterTest(unittest.TestCase):
    def setUp(self):
        self.arabic_number_converter = ArabicNumberConverter()

    def test_return_type_is_string(self):
        result = self.arabic_number_converter.convert_arabic_number_to_roman_numeral(0)
        nt.assert_true(type(result) is str)

    def test_numbers_1_through_9(self):
        nt.assert_equal(self.arabic_number_converter.convert_arabic_number_to_roman_numeral(3), "III")
        nt.assert_equal(self.arabic_number_converter.convert_arabic_number_to_roman_numeral(4), "IV")
        nt.assert_equal(self.arabic_number_converter.convert_arabic_number_to_roman_numeral(8), "VIII")

    def test_numbers_10_through_99(self):
        nt.assert_equal(self.arabic_number_converter.convert_arabic_number_to_roman_numeral(10), "X")
        nt.assert_equal(self.arabic_number_converter.convert_arabic_number_to_roman_numeral(30), "XXX")
        nt.assert_equal(self.arabic_number_converter.convert_arabic_number_to_roman_numeral(33), "XXXIII")
        nt.assert_equal(self.arabic_number_converter.convert_arabic_number_to_roman_numeral(90), "XC")
        nt.assert_equal(self.arabic_number_converter.convert_arabic_number_to_roman_numeral(25), "XXV")

    def test_numbers_100_through_999(self):
        nt.assert_equal(self.arabic_number_converter.convert_arabic_number_to_roman_numeral(333), "CCCXXXIII")
        nt.assert_equal(self.arabic_number_converter.convert_arabic_number_to_roman_numeral(759), "DCCLIX")
        nt.assert_equal(self.arabic_number_converter.convert_arabic_number_to_roman_numeral(999), "CMXCIX")

    def test_numbers_1000_through_9999(self):
        nt.assert_equal(self.arabic_number_converter.convert_arabic_number_to_roman_numeral(5478), "MMMMMCDLXXVIII")
        nt.assert_equal(self.arabic_number_converter.convert_arabic_number_to_roman_numeral(9999), "MMMMMMMMMCMXCIX")

    def test_numbers_higher_than_9999(self):
        nt.assert_equal(self.arabic_number_converter.convert_arabic_number_to_roman_numeral(10000),
                        "Your number is too large. Please select something between 1 and 10000.")